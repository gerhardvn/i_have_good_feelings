---
layout: default
---

![Banner](assets/title.jpg)

## Augmented reality storybook intervention
Augmented reality is an interactive experience that combines the real world and computer-generated content. The *I have good feelings* book uses an application to augment its content. The augmented reality application can be downloaded below. If your device does not support augmented reality then an second version of the application is provided with video files as alternative.

## About installing the application
To install the application on your device you will need to use sideloading.

Sideloading is the installation of an application on a mobile device without using the device's official application distribution method. 

The application is in APK format which is short for Android Package Kit. It's a file format for installing and distributing Android Apps. The user would download and install the APK file manually on their device. 

App sideloading on Android devices is also relatively straightforward. The user first downloads the APK file to the device. The user then taps on the file to install it, at which point the user is prompted to trust the app source. Once the source has been trusted, the app installation process continues.

## Installing the APK

### Allow Installation From Unknown Sources:  
The Play Store is the recommended app store for Android devices. So, as a security measure, your device will try to prevent sideloading apps from non-play store websites. You need to change this for successful sideloading. Go to Settings/Privacy> More/Advanced Settings and enable Install Unknown Apps.

### Download one of the APK files:
* [Augmented reality APK](https://drive.usercontent.google.com/download?id=1oRdspvdXF3ffRWPh_EXlX7DvWF9gzNv-)
* [Video APK](https://drive.usercontent.google.com/download?id=1UptJj0MFnl9L8Ps_nRzXq-Rl0TljldHF)

### Install the APK File:  
You can easily install the APK File now. Unless you decide otherwise, the APK file should be in the device’s Download folder. Open your Android’s file manager and select the APK file from its location. Tap on the file and follow the screen prompts to complete the installation process. 